//
//  EMIViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 22/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class EMIViewController: UIViewController {

    @IBOutlet weak var numberofmonthsTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var interestTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var loanAmountTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var calculateEMIButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var monthlyEMIAmountLabel: UILabel!
    @IBOutlet weak var enterDataAmountBGView: UIView!
    @IBOutlet weak var resultBGView: UIView!
    
    @IBOutlet weak var payInterestamountLabel: UILabel!
    @IBOutlet weak var totalPaymentLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

 
        
         self.resultBGView.isHidden = true
        self.enterDataAmountBGView.layer.cornerRadius = 6
        self.enterDataAmountBGView.layer.borderWidth = 1
        self.enterDataAmountBGView.layer.borderColor = UIColor.gray.cgColor
        
        self.resultBGView.layer.cornerRadius = 6
        self.resultBGView.layer.borderWidth = 1
        self.resultBGView.layer.borderColor = UIColor.gray.cgColor
       
            

       

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.closeButton.layer.cornerRadius = self.closeButton.frame.size.width/2
        self.calculateEMIButton.layer.cornerRadius = 5
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func calculateEMIButtonTapped(_ sender: UIButton) {
       
        if self.numberofmonthsTextField.text?.isEmpty != true && self.loanAmountTextfield.text?.isEmpty != true && self.interestTextField.text?.isEmpty != true {
            let loanAmount:Double = Double(self.loanAmountTextfield.text!)!
            let months:Double = Double(self.numberofmonthsTextField.text!)!
            let interest:Double = Double(self.interestTextField.text!)!
            let emi = calculateEmi(loanAmount, loanTenure: months, interestRate: interest)
            self.monthlyEMIAmountLabel.text = String(format:"%.2f", emi)+" /-"
            self.payInterestamountLabel.text = String(format:"%.2f",calculateTotalInterestPayable(emi*months, loanAmount: loanAmount))+" /-"
            self.totalPaymentLabel.text = String(format:"%.2f",emi*months)+" /-"
            print(emi)
            self.resultBGView.isHidden = false
            
        }else{
            SmartSecUtilities.sharedInstance.showAlertToUser(massege: "All Fields Are Mandatory", controller: self)
        }

        
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.numberofmonthsTextField.text = ""
        self.loanAmountTextfield.text = ""
        self.interestTextField.text = ""
        self.resultBGView.isHidden = true
    }
    func calculateEmi(_ loanAmount : Double, loanTenure : Double, interestRate : Double) -> Double {
        let interestRateVal = interestRate / 1200
        let loanTenureVal = loanTenure //* 12
        return loanAmount * interestRateVal / (1 - (pow(1/(1 + interestRateVal), loanTenureVal)))
    }
    
    func calculateTotalPayment(_ emi : Double, loanTenure : NSInteger) -> Double {
        let totalMonth = loanTenure// * 12
        return emi * Double(totalMonth)
    }
    
    func calculateTotalInterestPayable(_ totalPayment : Double, loanAmount : Double) -> Double {
        return totalPayment - loanAmount
    }

}
