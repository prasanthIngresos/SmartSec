//
//  FlashLightViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 22/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import AVFoundation
class FlashLightViewController: UIViewController {

    @IBOutlet weak var slideVierwOBJ: UISlider!
    var isFlashAnimation = true
    var flashAnnimationTime = 2.0
    var timer1:Timer?
    var timer2:Timer?
    var value: Float = 10.0
    override func viewDidLoad() {
        super.viewDidLoad()

         slideVierwOBJ.maximumValue = self.value

       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func flashButtonTapped(_ sender: UIButton) {
         self.toggleTorch(on: true)
    }
    
    @IBAction func flashAnimationButtonTapped(_ sender: UIButton) {
        
        self.timer2?.invalidate()
      //   self.timer1 = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(FlashLightViewController.flashAnimation), userInfo: nil, repeats: true)
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        //self.flashAnnimationTime = Double(sender.value)
        self.timer1?.invalidate()
        self.timer2?.invalidate()
      //  self.timer2 = Timer.scheduledTimer(timeInterval: TimeInterval(sender.value), target: self, selector: #selector(FlashLightViewController.flashAnimation), userInfo: nil, repeats: true)
       
        if sender.value < self.value {
            //sender.setValue(self.value, animated: false)
          
           
            self.value = self.value + sender.value
             print("\(self.value)")
        } else if sender.value > self.value{
//            self.value = slider.value
            
             self.value = self.value - sender.value
             print("\(self.value)")
            
        }
       
    }
    
    @objc func flashAnimation(){
        
        if isFlashAnimation == true {
            toggleTorch(on: true)
            isFlashAnimation = false
        }else{
            toggleTorch(on: false)
            isFlashAnimation = true
        }
        
        
    }
    
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else{
            return
        }
   
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }

}
