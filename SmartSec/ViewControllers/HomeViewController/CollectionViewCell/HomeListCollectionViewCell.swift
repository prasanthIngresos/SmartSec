//
//  HomeListCollectionViewCell.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 01/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class HomeListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var appiConImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
