//
//  HomeViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 01/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    
   
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()


        self.homeCollectionView.layer.cornerRadius = 20
        self.homeCollectionView.layer.borderColor = UIColor.lightGray.cgColor
        self.homeCollectionView.layer.borderWidth = 0.5
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "SmartSec"
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeListCollectionViewCell", for: indexPath) as! HomeListCollectionViewCell
        cell.appNameLabel.text = appListArray[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: UIScreen.main.bounds.size.width/4, height: 110.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let flashLightVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FlashLightViewController") as! FlashLightViewController
            self.title = ""
            self.navigationController?.pushViewController(flashLightVCOBJ, animated: true)
        }else if indexPath.item == 1 {
            let emiVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EMIViewController") as! EMIViewController
            self.title = ""
            self.navigationController?.pushViewController(emiVCOBJ, animated: true)
        }else if indexPath.item == 2 {
            let unitConverterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UnitConverterViewController") as! UnitConverterViewController
            self.title = ""
            self.navigationController?.pushViewController(unitConverterVC, animated: true)
        }else if indexPath.item == 3 {
//            let bookmarkListVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookmarksListViewController") as! BookmarksListViewController
//            self.navigationController?.pushViewController(bookmarkListVCOBJ, animated: true)
        }else if indexPath.item == 4 {
//            let bookmarkListVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookmarksListViewController") as! BookmarksListViewController
//            self.navigationController?.pushViewController(bookmarkListVCOBJ, animated: true)
        }else if indexPath.item == 5 {
            let calculatorVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalculatorViewController") as! CalculatorViewController
            self.title = ""
            self.navigationController?.pushViewController(calculatorVCOBJ, animated: true)
        }else if indexPath.item == 6 {
            let calendarVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
            self.title = ""
            self.navigationController?.pushViewController(calendarVCOBJ, animated: true)
        }else if indexPath.item == 7 {
            let remainderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RemainderViewController") as! RemainderViewController
             self.title = ""
            self.navigationController?.pushViewController(remainderVC, animated: true)
        }else if indexPath.item == 8 {
            let bookmarkListVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookmarksListViewController") as! BookmarksListViewController
            self.title = ""
            self.navigationController?.pushViewController(bookmarkListVCOBJ, animated: true)
        }else if indexPath.item == 9 {
            let budgetVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShowBudgetListViewController") as! ShowBudgetListViewController
            self.title = ""
            self.navigationController?.pushViewController(budgetVCOBJ, animated: true)
        }
    }
    @IBAction func menuButtonTapped(_ sender: UIBarButtonItem) {
    }
}






