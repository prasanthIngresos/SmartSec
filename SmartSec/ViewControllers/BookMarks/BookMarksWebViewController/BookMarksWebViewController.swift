//
//  BookMarksWebViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 01/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import WebKit
import SafariServices
import NVActivityIndicatorView
class BookMarksWebViewController: UIViewController,WKNavigationDelegate,SFSafariViewControllerDelegate,NVActivityIndicatorViewable,UIWebViewDelegate {

    @IBOutlet weak var bookmarkWebView1: UIWebView!
     var gameURL:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bookmarkWebView1.delegate = self
        print(gameURL!)
        if gameURL != nil {
            print(gameURL!)
            let url = NSURL (string: gameURL!);
            let request = URLRequest(url: url! as URL)
            bookmarkWebView1.loadRequest(request);
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    
        self.stopAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopAnimating()
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        self.startAnimating()
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.stopAnimating()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
