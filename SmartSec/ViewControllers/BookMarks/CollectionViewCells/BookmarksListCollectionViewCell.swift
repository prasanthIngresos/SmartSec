//
//  BookmarksListCollectionViewCell.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class BookmarksListCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bookmarkTitleLabel: UILabel!
    @IBOutlet weak var iConLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iConLabel.layer.cornerRadius = 15
        iConLabel.layer.borderWidth = 2
        iConLabel.layer.borderColor = UIColor.black.cgColor
//        iConLabel.backgroundColor = UIColor.blue
      //  iConLabel.textColor = UIColor.white
        iConLabel.clipsToBounds = true
        iConLabel.layer.shadowColor = UIColor.black.cgColor
        iConLabel.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        iConLabel.layer.masksToBounds = true
        iConLabel.layer.shadowRadius = 10.0
        iConLabel.layer.shadowOpacity = 0.5
  }
}
