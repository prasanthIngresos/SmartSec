//
//  BookMarksViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import Photos
import SkyFloatingLabelTextField
import CoreData
class BookMarksViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate {

    
    @IBOutlet weak var bookmarkiConLabel: UILabel!
    @IBOutlet weak var bookMarkButton: UIButton!
    @IBOutlet weak var bookmarkDescriptionTextFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var bookmarkURLtextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var bookmarkTitleTextfield: SkyFloatingLabelTextField!
   @IBOutlet weak var bookmarkImageView: UIImageView!
    var profileImageData:NSData?
    var isURL:Bool?
    var bookmarkTitleiCon = ""
    
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        bookmarkiConLabel.layer.cornerRadius = bookmarkiConLabel.frame.size.width / 2
        bookmarkiConLabel.layer.borderWidth = 2
        bookmarkiConLabel.layer.borderColor = UIColor.white.cgColor
        bookmarkiConLabel.backgroundColor = UIColor.blue
        bookmarkiConLabel.textColor = UIColor.white
        bookmarkiConLabel.clipsToBounds = true
        bookmarkiConLabel.layer.shadowColor = UIColor.black.cgColor
        bookmarkiConLabel.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        bookmarkiConLabel.layer.masksToBounds = true
        bookmarkiConLabel.layer.shadowRadius = 10.0
        bookmarkiConLabel.layer.shadowOpacity = 0.5
        self.bookmarkURLtextfield.text = "https://www.google.co.in"
       // self.customization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
 //MARK:- @customization
    func customization(){
        self.imagePicker.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BookMarksViewController.imageTapped(_:)))
         tapGestureRecognizer.numberOfTapsRequired = 1
        self.bookmarkImageView.addGestureRecognizer(tapGestureRecognizer)
        bookmarkImageView.layer.cornerRadius = bookmarkImageView.frame.size.width / 2
        bookmarkImageView.layer.borderWidth = 2
        bookmarkImageView.layer.borderColor = UIColor.white.cgColor
        bookmarkImageView.clipsToBounds = true
        bookmarkImageView.layer.shadowColor = UIColor.black.cgColor
        bookmarkImageView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        bookmarkImageView.layer.masksToBounds = true
        bookmarkImageView.layer.shadowRadius = 10.0
        bookmarkImageView.layer.shadowOpacity = 0.5
        bookmarkImageView.isUserInteractionEnabled = true
        
        //self.imagePicker.delegate = self
        
      
    }
      //MARK:- @imageTapped
    @objc func imageTapped(_ sender:AnyObject){
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- @IBAction
    @IBAction func savebookmarkButtonTapped(_ sender: UIButton) {
        var message = ""
        if self.bookmarkTitleTextfield.text?.isEmpty != true && self.bookmarkURLtextfield.text?.isEmpty != true && self.bookmarkDescriptionTextFiled.text?.isEmpty != true {
            if  self.verifyUrl (urlString: "http://"+self.bookmarkURLtextfield.text!) {
                
                let bookmarkOBJ:BookMarks  = NSEntityDescription.insertNewObject(forEntityName: "BookMarks", into: DataBaseController.persistentContainer.viewContext) as! BookMarks
                bookmarkOBJ.bookmarkTitle = self.bookmarkTitleTextfield.text!
                bookmarkOBJ.bookmarkURL = "http://"+self.bookmarkURLtextfield.text!
                bookmarkOBJ.bookmarkImage = String(self.bookmarkTitleTextfield.text!.prefix(1))
                bookmarkOBJ.bookMarkDescription = self.bookmarkDescriptionTextFiled.text!
                DataBaseController.saveContext()
              //  self.dismiss(animated: true, completion: nil)
            }else{
               
                message = "Invalid URL Pleasecheck it Once"
            }
        }else{
            message = "All Fields are mandatory"
        }
        if message.count > 0 {
            // show alert to user
            SmartSecUtilities.sharedInstance.showAlertToUser(massege: message, controller: self)
        }
    }
    
   //MARK:- @UIImagePickerControllerDelegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            //  self.composeMessage(type: .photo, content: pickedImage)
            self.bookmarkImageView.image  = pickedImage
            
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            // self.composeMessage(type: .photo, content: pickedImage)
            self.bookmarkImageView.image  = pickedImage
        }
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImageData = UIImageJPEGRepresentation(chosenImage, 0.5) as NSData?
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- @UITextFieldDelegates
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.bookmarkURLtextfield == textField {
          self.isURL = self.verifyUrl(urlString: self.bookmarkURLtextfield.text)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.bookmarkTitleTextfield == textField {
            if range.location == 0 {
               
                self.bookmarkTitleiCon = string
                self.bookmarkiConLabel.text = self.bookmarkTitleiCon.uppercased()
                
            }
        }
        return true
    }
    
    //MARK:- @touchesEvent
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         self.view.endEditing(true)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
         self.view.endEditing(true)
    }

    //MARK:- @verifyUrl
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
}
