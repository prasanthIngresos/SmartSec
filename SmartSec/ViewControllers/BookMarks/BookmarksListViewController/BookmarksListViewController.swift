//
//  BookmarksListViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
class BookmarksListViewController: UIViewController {

    @IBOutlet weak var bookmarksListCollectionView: UICollectionView!
    var bookmarksArray:[BookMarks] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let featchRequest:NSFetchRequest<BookMarks> = BookMarks.fetchRequest()
        do {
            
            self.bookmarksArray = try DataBaseController.persistentContainer.viewContext.fetch(featchRequest)
            
            self.bookmarksListCollectionView.reloadData()
        }catch let error {
            print(error.localizedDescription)
        }
    }

   
    @IBAction func addButtonTapped(_ sender: UIButton) {
        let bookmarkVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookMarksViewController") as! BookMarksViewController
       // self.present(bookmarkVCOBJ, animated: true, completion: nil)
        self.navigationController?.pushViewController(bookmarkVCOBJ, animated: true)
    }
    
}

extension BookmarksListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookmarksArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookmarksListCollectionViewCell", for: indexPath) as! BookmarksListCollectionViewCell
        
        let bookmarksOBJ = bookmarksArray[indexPath.item]
        cell.bookmarkTitleLabel.text = bookmarksOBJ.bookmarkTitle
        cell.iConLabel.text  = bookmarksOBJ.bookmarkImage?.uppercased()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bookmarkWebVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookMarksWebViewController") as! BookMarksWebViewController
        let bookmarksOBJ = bookmarksArray[indexPath.item]
        bookmarkWebVCOBJ.gameURL = bookmarksOBJ.bookmarkURL
        //self.present(bookmarkWebVCOBJ, animated: true, completion: nil)
        self.navigationController?.pushViewController(bookmarkWebVCOBJ, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: UIScreen.main.bounds.size.width/3.2, height: 110.0)
    }
    
    
    
}
