//
//  AddRemainderViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 05/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import UserNotifications
class AddRemainderViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var selectDateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var remainderTitleTextFields: SkyFloatingLabelTextField!
    @IBOutlet weak var remainderDescriptionTextField: SkyFloatingLabelTextField!
   
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()
    override func viewDidLoad() {
        super.viewDidLoad()

        createDatePicker()
        createToolBar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createDatePicker() {
         datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(datePicker:)), for: .valueChanged)
    }
    func createToolBar() {
        toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        let todayButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonPressed(sender:)))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([todayButton,flexibleSpace,doneButton], animated: true)
    }
 
    @objc func cancelButtonPressed(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonPressed(sender: UIBarButtonItem) {
         self.view.endEditing(true)
    }
    @objc func datePickerValueChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let ageSTR = dateFormatter.string(from: datePicker.date)
        self.selectDateTextField.text = ageSTR
        print(ageSTR)
   
    }
    @IBAction func saveButtonTapped(_ sender: RoundButton) {
        if self.remainderTitleTextFields.text?.isEmpty != true && self.remainderDescriptionTextField.text?.isEmpty != true && self.selectDateTextField.text?.isEmpty != true  {
            
            let pickerDate:Date = self.datePicker.date
            let time:TimeInterval = floor((pickerDate.timeIntervalSinceReferenceDate)/60.0 * 60.0)
            let notificationFireDate:Date = Date.init(timeIntervalSinceReferenceDate: time)
            let localNotification:UILocalNotification = UILocalNotification()
            localNotification.fireDate = notificationFireDate
            localNotification.repeatInterval = .day
            localNotification.alertBody = self.remainderDescriptionTextField.text
            localNotification.alertTitle = self.remainderTitleTextFields.text
            localNotification.timeZone = NSTimeZone.default
            localNotification.soundName = UILocalNotificationDefaultSoundName
            localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber+1
            UIApplication.shared.scheduleLocalNotification(localNotification)
            NotificationCenter.default.post(name: Notification.Name("reloadData"), object: self)

            self.navigationController?.popViewController(animated: true)
        }else{
            print("not remainder save")
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if selectDateTextField == textField {
            selectDateTextField.inputView = datePicker
            selectDateTextField.inputAccessoryView = toolBar
            selectDateTextField.text = ""
        }
        
        return true
    }
    
}
