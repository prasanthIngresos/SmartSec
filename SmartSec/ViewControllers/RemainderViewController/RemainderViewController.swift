//
//  RemainderViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 05/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import UserNotifications

class RemainderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let val:NSData? = nil
    var notificationsList:NSArray = NSArray()
    var localNotification:UILocalNotification?
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview.delegate = self
        self.tableview.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name(rawValue: "reloadData"), object: nil)
        let addButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addButtonTapped))
        let clearButton = UIBarButtonItem(title: "Clear All", style: .plain, target: self, action: #selector(notificationClearlButtonTapped))
       
        self.navigationItem.rightBarButtonItems = [addButton,clearButton]
    }

    @objc func reloadTableView(){
        self.tableview.reloadData()
    }
    @objc func notificationClearlButtonTapped(){
        UIApplication.shared.cancelAllLocalNotifications()
        self.tableview.reloadData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableview.reloadData()
    }
    @objc func addButtonTapped(){
        let addRemainderVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddRemainderViewController") as! AddRemainderViewController
        self.title = ""
        self.navigationController?.pushViewController(addRemainderVCOBJ, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (UIApplication.shared.scheduledLocalNotifications?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.notificationsList = UIApplication.shared.scheduledLocalNotifications! as NSArray
        self.localNotification = self.notificationsList[indexPath.row] as? UILocalNotification
        let cell = tableView.dequeueReusableCell(withIdentifier: "RemainderTableViewCell", for: indexPath) as! RemainderTableViewCell
        cell.notificationTitleLabel.text! = (localNotification?.alertBody)!
        cell.notificationDateLabel.text = self.getStringFromDate(date: (localNotification?.fireDate)!)
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let app = UIApplication.shared
        var eventArray: [Any]? = app.scheduledLocalNotifications
        let oneEvent = eventArray?[indexPath.row] as? UILocalNotification
        
        app.cancelLocalNotification(oneEvent!)
        self.tableview.reloadData()
        
        
        
    }
    
    func getStringFromDate(date:Date) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = NSTimeZone.default
        
        return dateFormatter.string(from:date)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        UIApplication.shared.cancelAllLocalNotifications()
        self.tableview.reloadData()
        
    }


}
