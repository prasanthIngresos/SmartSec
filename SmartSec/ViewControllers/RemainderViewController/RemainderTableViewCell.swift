//
//  RemainderTableViewCell.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 05/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class RemainderTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var notificationDateLabel: UILabel!
     @IBOutlet weak var notificationDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
