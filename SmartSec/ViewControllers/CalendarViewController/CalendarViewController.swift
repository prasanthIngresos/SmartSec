//
//  CalendarViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 02/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

         let today = NSDate()
         let timeSince = NSDate.timeIntervalSinceReferenceDate // this plus
         let todayToFutureDate = today.timeIntervalSince(today as Date)
         let finalInterval = todayToFutureDate + timeSince
         
         // UIApplication.shared.openURL(NSURL(string: "calshow:\(finalInterval)")! as URL)
         
         UIApplication.shared.open(NSURL(string: "calshow:\(finalInterval)")! as URL, options: [:]) { (isFinished) in
         print("Opened")
        } 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
