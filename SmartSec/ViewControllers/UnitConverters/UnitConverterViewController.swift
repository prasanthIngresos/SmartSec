//
//  UnitConverterViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 23/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class UnitConverterViewController: UIViewController,HADropDownDelegate {

    @IBOutlet weak var selectToView: HADropDown!
    @IBOutlet weak var selectFromView: HADropDown!
    @IBOutlet weak var selectCategoryView: HADropDown!
    @IBOutlet weak var amountTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var resultLabel: UILabel!
    let selectCategoryArray = ["Weight","Distance","Volume","Speed","Temperature"]
    
    var fromUnitMass:UnitMass?
    var toUnitMass:UnitMass?
    var fromUnitTemp:UnitTemperature?
    var toUnitTemp:UnitTemperature?
    var fromUnitSpeed:UnitSpeed?
    var toUnitSpeed:UnitSpeed?
    var fromUnitDistance:UnitLength?
    var toUnitDistance:UnitLength?
    var fromUnitVolume:UnitVolume?
    var toUnitVolume:UnitVolume?
    var selectTypeConcvertion = ""
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        selectCategoryView.items = self.selectCategoryArray
        selectCategoryView.title = "select type Convesion"
        selectFromView.title = "select type Convesion"
        selectToView.title = "select type Convesion"
        selectCategoryView.delegate = self
        selectFromView.delegate = self
        selectToView.delegate = self
        selectCategoryView.itemHeight = 65

        let distance = Measurement(value: 106.4, unit: UnitLength.kilometers)
        let distanceInMeters = distance.converted(to: .meters)
        print(distanceInMeters)
        
        let resultDistance = self.distanceConversion(Convertfrom: .kilometers, ConvertTO: .centimeters, quantity: 100)
        print(resultDistance)
        let resultData = self.weightConversion(Convertfrom: .kilograms, ConvertTO: .carats, quantity: 100)
        print(resultData)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    
    //MARK:- TouchEvents
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    //MARK:- weightConversion
    
    func weightConversion(Convertfrom:UnitMass,ConvertTO:UnitMass,quantity:Double) -> String{
        
        let weight = Measurement(value: quantity, unit: Convertfrom)
        let convertedWeight = String(describing: weight.converted(to: ConvertTO))
        return convertedWeight
        
    }
    //MARK:- distanceConversion
    func distanceConversion(Convertfrom:UnitLength,ConvertTO:UnitLength,quantity:Double) -> String{
        
        let distance = Measurement(value: quantity, unit: Convertfrom)
        let resultDistance = String(describing: distance.converted(to: ConvertTO))
        return resultDistance
        
    }
    //MARK:- volumeConversion
    func volumeConversion(Convertfrom:UnitVolume,ConvertTO:UnitVolume,quantity:Double) -> String{
        let Volume = Measurement(value: quantity, unit: Convertfrom)
        let resultVolume = String(describing: Volume.converted(to: ConvertTO))
        return resultVolume
    }
    //MARK:- speedConversion
    func speedConversion(Convertfrom:UnitSpeed,ConvertTO:UnitSpeed,quantity:Double) -> String{
        let speed = Measurement(value: quantity, unit: Convertfrom)
        let resultSpeed = String(describing: speed.converted(to: ConvertTO))
        return resultSpeed
    }
    //MARK:- temperatureConversion
    func temperatureConversion(Convertfrom:UnitTemperature,ConvertTO:UnitTemperature,quantity:Double) -> String{
        
        let temperature = Measurement(value: quantity, unit: Convertfrom)
        let resultTemperature = String(describing: temperature.converted(to: ConvertTO))
        return resultTemperature
        
    }
   //MARK:- temperatureConversion
    func didSelectItem(dropDown: HADropDown, at index: Int) {
       
        var selectTableName = ""
      
        if self.selectCategoryView == dropDown {
            
             print("Item selected at index \(index)")
            let selectCategory = selectCategoryArray[index]
            self.selectTypeConcvertion = selectCategory
            if selectCategory == "Weight" {
                
                self.selectFromView.items = weightConvertUnitsArray
                self.selectToView.items = weightConvertUnitsArray
                
            }else if selectCategory == "Distance" {
                self.selectFromView.items = distanceConversionArray
                self.selectToView.items = distanceConversionArray
                
            }else if selectCategory == "Volume" {
                self.selectFromView.items = volumeArray
                self.selectToView.items = volumeArray
                
            }else if selectCategory == "Speed" {
                self.selectFromView.items = speedConversionArray
                self.selectToView.items = speedConversionArray
             
                
            }else if selectCategory == "Temperature" {
                
                self.selectFromView.items = temperatureConversionArray
                self.selectToView.items = temperatureConversionArray
            }
            
            
        }else if self.selectToView == dropDown {
             print("Item selected at index \(index)")
            selectTableName = "selectToView"
            if self.selectTypeConcvertion == "Weight"  {
               let selectItem = weightConvertUnitsArray[index]
                 self.weightCalculation(whichTable: selectTableName, selectType: selectItem)
                
            }else if self.selectTypeConcvertion == "Distance" {
                let selectItem = distanceConversionArray[index]
                self.distanceCalculation(whichTable: selectTableName, selectType: selectItem)
                
            }else if self.selectTypeConcvertion == "Volume" {

                //selectItem = volumeConversionArray[index]
                let selectItem = volumeArray[index]
                self.volumeCalculation(whichTable: selectTableName, selectType: selectItem)
                
            }else if self.selectTypeConcvertion == "Speed" {
                //selectItem = speedConversionArray[index]
                let selectItem = speedConversionArray[index]
                self.speedCalculation(whichTable: selectTableName, selectType: selectItem)
                
            }else if self.selectTypeConcvertion == "Temperature" {
                //selectItem = temperatureConversionArray[index]
                let selectItem = temperatureConversionArray[index]
                self.temperatureCalculation(whichTable: selectTableName, selectType: selectItem)
                
            }
        }else if self.selectFromView == dropDown {
             print("Item selected at index \(index)")
            selectTableName = "selectFromView"
            if self.selectTypeConcvertion == "Weight"  {
              let selectItem = weightConvertUnitsArray[index]
                self.weightCalculation(whichTable: selectTableName, selectType: selectItem)
            }else if self.selectTypeConcvertion == "Distance" {
                let selectItem = distanceConversionArray[index]
                self.distanceCalculation(whichTable: selectTableName, selectType: selectItem)
            }else if self.selectTypeConcvertion == "Volume" {
                let selectItem = volumeArray[index]
                self.volumeCalculation(whichTable: selectTableName, selectType: selectItem)
               
            }else if self.selectTypeConcvertion == "Speed" {
                let selectItem = speedConversionArray[index]
                self.speedCalculation(whichTable: selectTableName, selectType: selectItem)
              
            }else if self.selectTypeConcvertion == "Temperature" {
                let selectItem = temperatureConversionArray[index]
                self.temperatureCalculation(whichTable: selectTableName, selectType: selectItem)
              
            }
        }
    }
    @IBAction func convertButtonTapped(_ sender: UIButton) {
       
        let alertMessage = ""
        selectFromView.title = "select type Convesion"
        selectToView.title = "select type Convesion"
        var resultVal = ""
        if self.selectTypeConcvertion == "Weight"  {
            if fromUnitMass != nil && toUnitMass != nil {
                
                resultVal = self.weightConversion(Convertfrom: fromUnitMass!, ConvertTO: toUnitMass!, quantity: 100)
                print(resultVal)
            }
           
        }else if self.selectTypeConcvertion == "Distance" {
            if fromUnitDistance != nil && toUnitDistance != nil {
 
                resultVal = self.distanceConversion(Convertfrom: fromUnitDistance!, ConvertTO: toUnitDistance!, quantity: 100)
               
            }
           
        }else if self.selectTypeConcvertion == "Volume" {
            if fromUnitVolume != nil && toUnitVolume != nil {
  
               resultVal = self.volumeConversion(Convertfrom: fromUnitVolume!, ConvertTO: toUnitVolume!, quantity: 200)
                print(resultVal)
            }

        }else if self.selectTypeConcvertion == "Speed" {
            if fromUnitSpeed != nil && toUnitSpeed != nil {
                resultVal = self.speedConversion(Convertfrom: fromUnitSpeed!, ConvertTO: toUnitSpeed!, quantity: 200)
                print(resultVal)
  
            }
           
           
        }else if self.selectTypeConcvertion == "Temperature" {
            if fromUnitTemp != nil && toUnitTemp != nil {
               resultVal = self.temperatureConversion(Convertfrom: fromUnitTemp!, ConvertTO: toUnitTemp!, quantity: 250)
                print(resultVal)
 
            }
        }
        self.resultLabel.text = resultVal
        if alertMessage.count > 0 {
            
        }
    }
    
    func weightCalculation(whichTable:String,selectType:String){
        
        if selectType == "kilograms" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.kilograms
                
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.kilograms
            }
        }
        if selectType == "grams" {
            if whichTable == "selectFromView" {
                 self.fromUnitMass = UnitMass.grams
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.grams
                
            }
        }
        if selectType == "decigrams" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.decigrams
                
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.decigrams
                
            }
        }
        if selectType == "centigrams" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.centigrams
                
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.centigrams
                
            }
        }
        if selectType == "milligrams" {
            if whichTable == "selectFromView" {
                 self.fromUnitMass = UnitMass.milligrams
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.milligrams
                
            }
        }
        if selectType == "micrograms" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.micrograms
                
            }else if whichTable == "selectToView"{
                 self.toUnitMass = UnitMass.micrograms
                
            }
        }
        if selectType == "nanograms" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.nanograms
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.nanograms
                
            }
        }
        if selectType == "picograms" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.picograms
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.picograms
            }
        }
        if selectType == "ounces" {
            if whichTable == "selectFromView" {
                 self.fromUnitMass = UnitMass.ounces
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.ounces
                
            }
        }
        if selectType == "pounds" {
            if whichTable == "selectFromView" {
                 self.fromUnitMass = UnitMass.pounds
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.pounds
            }
        }
        if selectType == "stones" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.stones
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.stones
            }
        }
        if selectType == "metricTons" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.metricTons
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.metricTons
            }
        }
        if selectType == "carats" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.carats
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.carats
            }
        }
        if selectType == "ouncesTroy" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.ouncesTroy
            }else if whichTable == "selectToView"{
                 self.toUnitMass = UnitMass.ouncesTroy
            }
        }
        if selectType == "slugs" {
            if whichTable == "selectFromView" {
                self.fromUnitMass = UnitMass.slugs
            }else if whichTable == "selectToView"{
                self.toUnitMass = UnitMass.slugs
            }
        }
        
        
    }
    
     func temperatureCalculation(whichTable:String,selectType:String){
      //  ["kelvin","celsius","fahrenheit"]
        
        if selectType == "kelvin" {
            if whichTable == "selectFromView" {
                self.fromUnitTemp = UnitTemperature.kelvin
            }else if whichTable == "selectToView"{
                self.toUnitTemp = UnitTemperature.kelvin
            }
        }
        if selectType == "celsius" {
            if whichTable == "selectFromView" {
               self.fromUnitTemp = UnitTemperature.celsius
            }else if whichTable == "selectToView"{
                self.toUnitTemp = UnitTemperature.celsius
            }
        }
        if selectType == "fahrenheit" {
            if whichTable == "selectFromView" {
               self.fromUnitTemp = UnitTemperature.fahrenheit
            }else if whichTable == "selectToView"{
                self.toUnitTemp = UnitTemperature.fahrenheit
            }
        }
    }
     func speedCalculation(whichTable:String,selectType:String){
// ["metersPerSecond","kilometersPerHour","milesPerHour","knots"]
        
        if selectType == "metersPerSecond" {
            if whichTable == "selectFromView" {
                self.fromUnitSpeed = UnitSpeed.metersPerSecond
            }else if whichTable == "selectToView"{
                self.toUnitSpeed = UnitSpeed.metersPerSecond
            }
        }
        if selectType == "kilometersPerHour" {
            if whichTable == "selectFromView" {
                self.fromUnitSpeed = UnitSpeed.kilometersPerHour
            }else if whichTable == "selectToView"{
                self.toUnitSpeed = UnitSpeed.kilometersPerHour
            }
        }
        if selectType == "milesPerHour" {
            if whichTable == "selectFromView" {
                self.fromUnitSpeed = UnitSpeed.milesPerHour
            }else if whichTable == "selectToView"{
                self.toUnitSpeed = UnitSpeed.milesPerHour
            }
        }
        if selectType == "knots" {
            if whichTable == "selectFromView" {
                self.fromUnitSpeed = UnitSpeed.knots
            }else if whichTable == "selectToView"{
                self.toUnitSpeed = UnitSpeed.knots
            }
        }
    }
    
    //["megameters","kilometers","hectometers","decameters","meters","centimeters","nanometers","picometers","inches","feet","yards","miles","scandinavianMiles","lightyears","nauticalMiles","fathoms","furlongs","astronomicalUnits","parsecs"]
     func distanceCalculation(whichTable:String,selectType:String){
        if selectType == "megameters" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.megameters
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.megameters
            }
        }
        if selectType == "kilometers" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.kilometers
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.kilometers
            }
        }
        if selectType == "hectometers" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.hectometers
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.hectometers
            }
        }
        if selectType == "decameters" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.decameters
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.decameters
            }
        }
        if selectType == "meters" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.meters
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.meters
            }
        }
        if selectType == "nanometers" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.nanometers
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.nanometers
            }
        }
        if selectType == "picometers" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.picometers
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.picometers
            }
        }
        if selectType == "inches" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.inches
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.inches
            }
        }
        if selectType == "feet" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.feet
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.feet
            }
        }
        if selectType == "yards" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.yards
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.yards
            }
        }
        if selectType == "miles" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.miles
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.miles
            }
        }
        if selectType == "scandinavianMiles" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.scandinavianMiles
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.scandinavianMiles
            }
        }
        if selectType == "lightyears" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.lightyears
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.lightyears
            }
        }
        if selectType == "nauticalMiles" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.nauticalMiles
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.nauticalMiles
            }
        }
        if selectType == "fathoms" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.fathoms
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.fathoms
            }
        }
        if selectType == "furlongs" {
            if whichTable == "selectFromView" {
                 self.fromUnitDistance = UnitLength.furlongs
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.furlongs
            }
        }
        if selectType == "astronomicalUnits" {
            if whichTable == "selectFromView" {
                self.fromUnitDistance = UnitLength.astronomicalUnits
            }else if whichTable == "selectToView"{
               self.toUnitDistance = UnitLength.astronomicalUnits
            }
        }
        if selectType == "parsecs" {
            if whichTable == "selectFromView" {
               self.fromUnitDistance = UnitLength.parsecs
            }else if whichTable == "selectToView"{
                self.toUnitDistance = UnitLength.parsecs
            }
        }

    }
    //let volumeArray = ["liters","milliliters","gallons","fluidOunces"]
    
    func volumeCalculation(whichTable:String,selectType:String){
        if selectType == "liters" {
            if whichTable == "selectFromView" {
                self.fromUnitVolume = UnitVolume.liters
            }else if whichTable == "selectToView"{
                self.toUnitVolume = UnitVolume.liters
            }
        }
        if selectType == "milliliters" {
            if whichTable == "selectFromView" {
                self.fromUnitVolume = UnitVolume.milliliters
            }else if whichTable == "selectToView"{
                self.toUnitVolume = UnitVolume.milliliters
            }
        }
        if selectType == "gallons" {
            if whichTable == "selectFromView" {
                self.fromUnitVolume = UnitVolume.gallons
            }else if whichTable == "selectToView"{
                self.toUnitVolume = UnitVolume.gallons
            }
        }
        if selectType == "fluidOunces" {
            if whichTable == "selectFromView" {
                self.fromUnitVolume = UnitVolume.fluidOunces
            }else if whichTable == "selectToView"{
                self.toUnitVolume = UnitVolume.fluidOunces
            }
        }
    }
}
