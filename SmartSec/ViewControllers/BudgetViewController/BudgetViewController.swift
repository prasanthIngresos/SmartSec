//
//  BudgetViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
import SkyFloatingLabelTextField
class BudgetViewController: UIViewController {

    @IBOutlet weak var dateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var amountTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var debitButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    
    
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()
    var selectDate:NSDate?
    var creditORdebitSTR = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.createDatePicker()
        self.createToolBar()
        dateTextField.inputView = datePicker
        dateTextField.inputAccessoryView = toolBar
        
    }
    func createDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(datePicker:)), for: .valueChanged)
    }
    @objc func datePickerValueChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.selectDate = datePicker.date as NSDate
        let dateSTR = dateFormatter.string(from: datePicker.date)
        self.dateTextField.text = dateSTR
        print(dateSTR)

    }
    
    func createToolBar() {
        toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        let todayButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonPressed(sender:)))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([todayButton,flexibleSpace,doneButton], animated: true)
        
    }
    @objc func cancelButtonPressed(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonPressed(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func creditButtonTapped(_ sender: UIButton) {
        self.creditButton.setImage(UIImage(named: "selected_circle"), for: .normal)
        self.debitButton.setImage(UIImage(named: "unselected_Circle"), for: .normal)
        self.creditORdebitSTR = "Credit"
    }
    
    @IBAction func debitButtonTapped(_ sender: UIButton) {
        self.creditButton.setImage(UIImage(named: "unselected_Circle"), for: .normal)
         self.debitButton.setImage(UIImage(named: "selected_circle"), for: .normal)
        self.creditORdebitSTR = "Debit"
        
    }
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.nameTextField.text?.isEmpty != true &&  self.amountTextField.text?.isEmpty != true &&  self.dateTextField.text?.isEmpty != true && self.creditORdebitSTR.isEmpty != true && self.creditORdebitSTR != "" && self.selectDate != nil  {
            
            let budgetOBJ:Budget  = NSEntityDescription.insertNewObject(forEntityName: "Budget", into: DataBaseController.persistentContainer.viewContext) as! Budget
            budgetOBJ.name = self.nameTextField.text!
            budgetOBJ.amount = self.amountTextField.text!
            budgetOBJ.budgetDate = self.selectDate
            budgetOBJ.creditORDebit = self.creditORdebitSTR
            DataBaseController.saveContext()
            self.nameTextField.text = ""
            self.amountTextField.text = ""
            self.dateTextField.text = "01/02/2018"
             self.fetchData()
            self.dismiss(animated: true, completion: nil)
        }else{
            SmartSecUtilities.sharedInstance.showAlertToUser(massege: "All Fields Are Mandetory", controller: self)
            
        }
        
       
        
    }
    
    func fetchData(){
        let FeatchRequiest:NSFetchRequest<Budget> = Budget.fetchRequest()
        print(FeatchRequiest)
        do{
            let budget = try DataBaseController.persistentContainer.viewContext.fetch(FeatchRequiest)
            
            for each in budget as [Budget] {
                print("empDetails :- \(each.name!)\n\(each.amount!)\n\(each.budgetDate!)")
            }
        }catch{
            print(" error\(error.localizedDescription)")
        }
    }
    

}
