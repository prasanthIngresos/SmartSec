//
//  ShowBudgetListViewController.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
class ShowBudgetListViewController: UIViewController {


    @IBOutlet weak var showBudgetTableView: UITableView!
    var budgetListArray:[Budget] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let FeatchRequiest:NSFetchRequest<Budget> = Budget.fetchRequest()
        print(FeatchRequiest)
        do{
            self.budgetListArray = try DataBaseController.persistentContainer.viewContext.fetch(FeatchRequiest)
            
            for each in budgetListArray as [Budget] {
                print("empDetails :- \(each.name!)\n\(each.amount!)\n\(each.budgetDate!)")
            }
            self.showBudgetTableView.reloadData()
        }catch{
            print(" error\(error.localizedDescription)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        let budgetVCOBJ = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BudgetViewController") as! BudgetViewController
       // self.present(budgetVCOBJ, animated: true, completion: nil)
        self.navigationController?.pushViewController(budgetVCOBJ, animated: true)
    }
    


}
extension ShowBudgetListViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return budgetListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetTableViewCell") as! BudgetTableViewCell
        let budgetArray:[Budget] = self.budgetListArray.reversed()
        let budget = budgetArray[indexPath.row]
        
        cell.userNameLabel.text = budget.name
        cell.budgetLabel.text = budget.amount
  
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let stringDate: String = dateFormatter.string(from: budget.budgetDate! as Date)
        print(stringDate)
        
        cell.dateLabel.text = "\(stringDate)"
        cell.statusLabel.text = budget.creditORDebit
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
