//
//  BudgetTableViewCell.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class BudgetTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundViewOBJ: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       // self.backgroundViewOBJ.layer.borderColor = UIColor.darkGray.cgColor
        self.backgroundViewOBJ.layer.cornerRadius = 5
        self.backgroundViewOBJ.layer.shadowColor = UIColor.black.cgColor
        self.backgroundViewOBJ.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.backgroundViewOBJ.layer.masksToBounds = false
        self.backgroundViewOBJ.layer.shadowRadius = 5.0
        self.backgroundViewOBJ.layer.shadowOpacity = 0.5
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
