//
//  BookMarks+CoreDataProperties.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 01/02/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//
//

import Foundation
import CoreData


extension BookMarks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookMarks> {
        return NSFetchRequest<BookMarks>(entityName: "BookMarks")
    }

    @NSManaged public var bookmarkTitle: String?
    @NSManaged public var bookmarkURL: String?
    @NSManaged public var bookmarkImage: String?
    @NSManaged public var bookMarkDescription: String?

}
