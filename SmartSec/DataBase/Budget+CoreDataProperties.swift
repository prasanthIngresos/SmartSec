//
//  Budget+CoreDataProperties.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//
//

import Foundation
import CoreData


extension Budget {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Budget> {
        return NSFetchRequest<Budget>(entityName: "Budget")
    }

    @NSManaged public var name: String?
    @NSManaged public var amount: String?
    @NSManaged public var budgetDate: NSDate?
    @NSManaged public var creditORDebit: String?

}
