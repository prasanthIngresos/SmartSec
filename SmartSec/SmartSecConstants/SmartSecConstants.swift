
//
//  SmartSecConstants.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 23/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

let weightConvertUnitsArray = ["kilograms","grams","decigrams","centigrams","milligrams","micrograms","nanograms","picograms","ounces","pounds","stones","metricTons","carats","ouncesTroy","slugs"]

let distanceConversionArray = ["megameters","kilometers","hectometers","decameters","meters","centimeters","nanometers","picometers","inches","feet","yards","miles","scandinavianMiles","lightyears","nauticalMiles","fathoms","furlongs","astronomicalUnits","parsecs"]
let volumeConversionArray = ["megaliters","kiloliters","liters","deciliters","centiliters","milliliters","cubicKilometers","cubicMeters","cubicDecimeters","cubicCentimeters","cubicMillimeters","cubicInches","cubicFeet","cubicYards","cubicMiles","acreFeet","bushels","teaspoons","tablespoons","fluidOunces","cups","pints","quarts","gallons","imperialTeaspoons","imperialTablespoons","imperialFluidOunces","imperialPints","imperialQuarts","imperialGallons","metricCups"]

let volumeArray = ["liters","milliliters","gallons","fluidOunces"]
let speedConversionArray = ["metersPerSecond","kilometersPerHour","milesPerHour","knots"]

let temperatureConversionArray = ["kelvin","celsius","fahrenheit"]
let appListArray = ["FlashLight","Loan Calculator","Unit Converter","Currency Converter","Note","Calculator","Calendar","Remainders","BookMarks","Budget"]
