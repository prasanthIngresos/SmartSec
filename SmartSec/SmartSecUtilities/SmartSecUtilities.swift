//
//  SmartSecUtilities.swift
//  SmartSec
//
//  Created by Ingresos Pvt Ltd on 31/01/18.
//  Copyright © 2018 Ingresos Pvt Ltd. All rights reserved.
//

import UIKit

class SmartSecUtilities: NSObject {
    
    class var sharedInstance :  SmartSecUtilities
    {
        struct Static
        {
            static let instance : SmartSecUtilities =  SmartSecUtilities()
        }
        return Static.instance
    }
    func showAlertToUser(massege:String,controller:UIViewController){
        let alertController = UIAlertController(title: "SmartSEc APP", message: massege, preferredStyle: .alert)
        controller.present(alertController, animated: true, completion: nil)
        let OKAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
    }

}
